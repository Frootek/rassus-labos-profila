package hr.fer.rassus.rest;

public class Aggregate {
	
	private long tempId;
	private int tempValue;
	
	private long humidityId;
	private int humidityValue;
	
	protected Aggregate() {}
	
	public Aggregate(long tempId, int tempValue, long humidityId, int humidityValue) {
		super();
		this.tempId = tempId;
		this.tempValue = tempValue;
		this.humidityId = humidityId;
		this.humidityValue = humidityValue;
	}
	public long getTempId() {
		return tempId;
	}
	public int getTempValue() {
		return tempValue;
	}
	public long getHumidityId() {
		return humidityId;
	}
	public int getHumidityValue() {
		return humidityValue;
	}
	
	
	
	
	
}
