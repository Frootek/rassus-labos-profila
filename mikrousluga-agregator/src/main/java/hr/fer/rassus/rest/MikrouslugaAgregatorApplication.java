package hr.fer.rassus.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;

@EnableEurekaClient
@SpringBootApplication
public class MikrouslugaAgregatorApplication {

	private static final Logger log = LoggerFactory.getLogger(MikrouslugaAgregatorApplication.class);
	
	public static void main(String[] args) {
		SpringApplication.run(MikrouslugaAgregatorApplication.class, args);
	}
	@Bean
	public CommandLineRunner commandLineRunner() 
	{
		return args -> {
		   log.info("Mikrousluga agregator je online!");
		 		   
		};
	}
}
