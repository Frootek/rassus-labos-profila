package hr.fer.rassus.rest;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;


/**
 * Class containing request mappings
 * @author Marin Juric
 *
 */

@RefreshScope
@RestController
@RequestMapping("")
public class WebService {
	
	/**
	 * For the time being we are using static urls for web service temperature and humidity
	 * 8080	-	temperature
	 * 8081	-	humidity
	 * 
	 * ----UPDATE-----
	 * Added dynamic ip addresses and ports depending on their setup in configuration microservice
	 */
	
	
	/**
	 * Deprecated, started using registration server!
	 */
//	@Value("${temperature_ip_address}")
//	private String temperature_ip_address;
//	
//	@Value("${temperature_port}")
//	private String temperature_port;
//
	@Value("${port:8082}")
	private String port;
	
	@Value("${ip_address:127.0.0.1}")
	private String address;
	
	@Value("${humidity-ms:mikrousluga-vlaga}")
	private String humidity_ms;
	
	@Value("${temperature-ms:mikrousluga-temperatura}")
	private String temperature_ms;
	
	@Value("${measurement_unit}")
	private String measurementUnit;
	
	@Autowired
  	private DiscoveryClient discoveryClient;

	protected WebService() {};
	
	/**
	 * Mapping that returns aggregate current-reading based on microservices 
	 * temperature and humidity
	 * First finds both humidity and temperature microservice using registration service
	 * 'eureka-server'. If one of the services or both can not be found returns null.
	 * Names of services are injected using configuration server.
	 * @return
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@GetMapping(path = "/readings")
	public Aggregate getCurrentReading() throws IOException, InterruptedException {
		
		HttpClient client = HttpClient.newHttpClient();

		//get humidity and temperature URLS by nicely asking our discovery server
		ServiceInstance humidity = discoveryClient.getInstances(humidity_ms).get(0);
		ServiceInstance temperature = discoveryClient.getInstances(temperature_ms).get(0);
		
	
		String TEMPERATURE_SERVICE_URL = 
				"http://"+ temperature.getHost() + ":"+ temperature.getPort() +"/current-reading";

		String HUMIDITY_SERVICE_URL = 
				"http://"+ humidity.getHost() + ":"+ humidity.getPort() +"/current-reading";
		
		ObjectMapper objectMapper = new ObjectMapper();
		
		//first create http-get request to temperature microservice
		
		HttpRequest requestTemp = HttpRequest.newBuilder()
				.uri(URI.create(TEMPERATURE_SERVICE_URL))
				.GET().build();
		
		HttpResponse<String> responseTemp = client.send(requestTemp, 
				HttpResponse.BodyHandlers.ofString());
		
		JsonNode jsonNode = objectMapper.readTree(responseTemp.body());
		long tempId = jsonNode.get("id").asLong();
		int tempValue = jsonNode.get("value").asInt();
		
		
		//check if configuration service requested Kelvin measurement units
		
		if(this.measurementUnit.equals("Kelvin"))
		{
			tempValue+= 273;
		}
		
		//create http-get request to humidity microservice
		
		HttpRequest requestHumidity = HttpRequest.newBuilder()
				.uri(URI.create(HUMIDITY_SERVICE_URL))
				.GET().build();
		
		HttpResponse<String> responseHumidity = client.send(requestHumidity, 
				HttpResponse.BodyHandlers.ofString());
		
		jsonNode = objectMapper.readTree(responseHumidity.body());
		long humidityId = jsonNode.get("id").asLong();
		int humidityValue = jsonNode.get("value").asInt();
		
		Aggregate aggregate = new Aggregate(tempId, tempValue, humidityId, humidityValue);		
		
		return aggregate;
			
	}
	
	
//	@GetMapping(path = "/refresh")
//	public HttpResponse<String> refreshActuator() throws IOException, InterruptedException
//	{
//		HttpClient client = HttpClient.newHttpClient();
//		
//		String myActuatorUri = "http://"+ this.address + ":"+ this.port +"/actuator/refresh";
//		
//		
//		HttpRequest requestRefresh = HttpRequest.newBuilder()
//		.uri(URI.create(myActuatorUri))
//		.POST(HttpRequest.BodyPublishers.ofString("")).build();
//
//		HttpResponse<String> response = client.send(requestRefresh, HttpResponse.BodyHandlers.ofString());
//		
//		return response;
//	}
}
