package hr.fer.rassus.model;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

/**
 * Factory class
 * Implements logistics of filling h2 database with temperatures
 * @author Marin Juric
 *
 */

public class Factory {
	
	/**
	 * Reads through each line (skips first line) of file "mjerenja.csv".
	 * and saves it to @HumidityRepository
	 * @param repository
	 * @throws IOException
	 */
	public static  void HumidityFactory(HumidityRepository repository) throws IOException
	{	
		String csvFile = "mjerenja.txt";
		BufferedReader br = null;
		String line = "";
		
		br = new BufferedReader(new FileReader(csvFile));
		br.readLine();
		String[] parts;
		
		while((line = br.readLine())!=null)
		{
			parts  = line.split(",");
			line = parts[2];
			repository.save(new HumidityMeasurement(Integer.parseInt(line)));
		}
		
		br.close();
	}

}
