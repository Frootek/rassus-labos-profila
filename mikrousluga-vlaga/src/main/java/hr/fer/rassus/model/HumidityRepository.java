package hr.fer.rassus.model;

import org.springframework.data.jpa.repository.JpaRepository;

public interface HumidityRepository extends JpaRepository<HumidityMeasurement, Long>{
	HumidityMeasurement findById(long id);
}
