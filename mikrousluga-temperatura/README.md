# Mikrousluga-temperatura

Mikrousluga-temperatura je jedna od mikrousluga koristena u okviru laboratorijske
vje�be iz kolegija "Raspodijeljeni sustavi".

Mikrousluga	sluzi za dohvat trenutnog mjerenja temperature.

## Koristenje

Nakon kloniranja repozitorija potrebno je pokrenuti build.gradle.
Eclipse: desni klik na datoteku build.gradle -> gradle -> refresh gradle project.

Pokrenuti MikrouslugaTemperaturaApplication kao java aplikaciju.

Usluzi se pristupa na portu definiranom u application.properties. (http://localhost:8080)

Mappings:
 - /hello
 
 
 - /h2-console
    - pristup H2 in memory bazi podataka
 	
 	
 - /current-reading
    - vraca trenutno mjerenje temperature definirano zadanom formulom

Nakon podizanja servera H2 baza se nalazi na [http://localhost:8080/h2-console](http://localhost:8080/h2-console).


Podatci za pristup definiriani su u datoteci application.properties.



