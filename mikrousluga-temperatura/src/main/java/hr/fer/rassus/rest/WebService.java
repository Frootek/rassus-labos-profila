package hr.fer.rassus.rest;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hr.fer.rassus.model.TemperatureMeasurement;
import hr.fer.rassus.model.TemperatureRepository;

/**
 * Class containing request mappings
 * @author Marin Juric
 *
 */

@RestController
@RequestMapping("")
public class WebService {
	
	
	protected WebService() {};
	
	private TemperatureRepository repository;
	
	@Autowired
	public void setRepository(TemperatureRepository repository) {
		this.repository = repository;
	}
	
	/**
	 * Mapping that returns current-reading based on following formula:
	 * ID = 4 * Sat(trenutno_vrijeme) + Minuta(trenutno_vrijeme) / 15
	 * @return
	 */
	@GetMapping(path = "/current-reading")
	public TemperatureMeasurement getCurrentReading() {
		LocalDateTime now = LocalDateTime.now();
		
		int id = 4 * now.getHour() + now.getMinute() / 15;
		
		TemperatureMeasurement measurement = this.repository.findById(id);
		//System.out.println(measurement.toString());
		
		return measurement;
		
			
	}
	
	


}
