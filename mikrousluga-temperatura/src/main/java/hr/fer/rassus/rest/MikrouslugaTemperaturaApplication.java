package hr.fer.rassus.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import hr.fer.rassus.model.Factory;
import hr.fer.rassus.model.TemperatureRepository;

@EnableEurekaClient
@SpringBootApplication
@EnableJpaRepositories(basePackages={"hr.fer.rassus.model"})
@ComponentScan("hr.fer.rassus.model")
@ComponentScan("hr.fer.rassus.rest")
@EntityScan("hr.fer.rassus.model")
public class MikrouslugaTemperaturaApplication {

	private static final Logger log = LoggerFactory.getLogger(MikrouslugaTemperaturaApplication.class);
	

	public static void main(String[] args) {
		SpringApplication.run(MikrouslugaTemperaturaApplication.class, args);
	}
	
	@Bean
	public CommandLineRunner commandLineRunner(TemperatureRepository repository) 
	{
		return args -> {
		   log.info("Mikrousluga temperatura je online!");
		 
		   log.info("Filling database...");
		   Factory.TemperatureFactory(repository);
		   
		};
	}

}
