package hr.fer.rassus.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@RefreshScope
@RestController
public class HelloWorldController {
	
	  @Autowired
	  private DiscoveryClient discoveryClient;
	
	  //@Value("${spring.application.name}")
	  private String appName = "mikrousluga-vlaga";
	
	  
	  @RequestMapping("/greet")
	    public List<ServiceInstance> greeting() {
	        return this.discoveryClient.getInstances(appName);
	    }
	  @RequestMapping("/greetInfo")
	    public String greetingIp() {
		  
		 String rez = 
		 this.discoveryClient.getInstances(appName).get(0).getHost() + "   " 
		 +this.discoveryClient.getInstances(appName).get(0).getPort()+ "  " 
		 +this.discoveryClient.getInstances(appName).get(0).getUri();
	        return rez;
	    }
	  
	  
	  
	  
	  @Value("${message:hello default}")
	  private String message;

	  @RequestMapping("/hello")
	  String getMessage() {
	    return this.message;
	  }
	
	
}
