package hr.fer.rassus.model;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TemperatureRepository extends JpaRepository<TemperatureMeasurement, Long>{
	TemperatureMeasurement findById(long id);
}
