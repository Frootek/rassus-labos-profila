package hr.fer.rassus.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class TemperatureMeasurement {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private Long id;
	private int value;
	
	protected TemperatureMeasurement(){}
	
	public TemperatureMeasurement(int value) {
		super();
		this.value = value;
	}

	@Override
	public String toString() {
		return "TemperatureMeasurement [id=" + id + ", value=" + value + "]";
	}

	public Long getId() {
		return id;
	}

	public int getValue() {
		return value;
	}
}
